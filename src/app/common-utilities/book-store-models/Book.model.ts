export class Book {
    id: number = 0;
    title: string = "";
    price: number = 0;
    format: string = "";
    publisher: string = "";
    publish_date: string = "";
    edition: string = "";
    pages: number = 0;
    dimension: string = "";
    overview: string = "";
    synopsis: string = "";
    is_active: boolean = false;
    effective_date: string = "";
    expiration_date: string = "";
    author_id: number = 0;
}
