import { Author } from "./../book-store-models/Author.model";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Footer } from "src/app/common-utilities/book-store-models/Footer.model";
import { Header } from "src/app/common-utilities/book-store-models/Header.model";
import { Book } from "../book-store-models/Book.model";
import { Category } from "src/app/common-utilities/book-store-models/Category.model";
@Injectable({
    providedIn: "root",
})
export class BookStoreCommonService {
    constructor(private httpClient: HttpClient) {}

    /**
     * Gets header data
     * @returns header data
     */
    getHeaderData(): Observable<Header> {
        return this.httpClient.get<Header>("/api/header");
    }

    /**
     * Gets footer data
     * @returns footer data
     */
    getFooterData(): Observable<Footer> {
        return this.httpClient.get<Footer>("/api/footer");
    }

    /**
     * Gets all books
     * @returns all books
     */
    getAllBooks(): Observable<Book[]> {
        return this.httpClient.get<Book[]>("/api/book");
    }
    /**
     * Gets category data
     * @returns category data
     */
    getCategoryData(): Observable<Category> {
        return this.httpClient.get<any>(`/api/category`);
    }

    /**
     * Gets Authors data
     * @returns Authors data
     */
    getAuthorsData(): Observable<Author[]> {
        return this.httpClient.get<any>("https://master.d2xgoyqekfrbk4.amplifyapp.com/?url=http://bookstore.ap-south-1.elasticbeanstalk.com/api/author");
    }
}
