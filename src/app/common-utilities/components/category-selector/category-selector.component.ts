import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";
import { Header } from "../../book-store-models/Header.model";
import { ERROR_MESSAGE } from "../../constants/errors.constants";
import { BookStoreCommonService } from "../../services/book-store-common.service";

@Component({
    selector: "app-category-selector",
    templateUrl: "./category-selector.component.html",
    styleUrls: ["./category-selector.component.scss"],
})
export class CategorySelectorComponent implements OnInit, OnDestroy {
    @Input() sidenav;
    header: Header = new Header();
    subscription: Subscription = new Subscription();
    error: string;

    constructor(private commonService: BookStoreCommonService) {}

    /**
     * on init
     */
    ngOnInit(): void {
        this.getHeaderData();
    }

    /**
     * Gets header data
     */
    getHeaderData(): void {
        this.subscription = this.commonService.getHeaderData().subscribe(
            (data) => {
                this.header = data;
            },
            (error) => (this.error = ERROR_MESSAGE)
        );
    }

    /**
     * on destroy
     */
    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
