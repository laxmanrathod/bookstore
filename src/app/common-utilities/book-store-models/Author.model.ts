export class Author {
    id: Number;
    name: String;
    biography?: String;
    is_active?: Boolean;
    effective_date?: String;
    expiration_date?: String;
    noOfBooks: Number;
}
