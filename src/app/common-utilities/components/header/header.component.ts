import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { Subscriber, Subscription } from "rxjs";
import { Header } from "src/app/common-utilities/book-store-models/Header.model";
import { ERROR_MESSAGE } from "../../constants/errors.constants";
import { BookStoreCommonService } from "../../services/book-store-common.service";

@Component({
    selector: "app-header",
    templateUrl: "./header.component.html",
    styleUrls: ["./header.component.scss"],
})
export class HeaderComponent implements OnInit, OnDestroy {
    @Input() sidenav;
    header: Header = new Header();
    subscription: Subscription = new Subscription();
    error: string;

    constructor(private commonService: BookStoreCommonService) {}

    /**
     * on init
     */
    ngOnInit(): void {
        this.getHeaderData();
    }

    /**
     * Gets header data
     */
    getHeaderData(): void {
        this.subscription = this.commonService.getHeaderData().subscribe(
            (data) => {
                this.header = data;
            },
            (error) => (this.error = ERROR_MESSAGE)
        );
    }

    /**
     * on destroy
     */
    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
