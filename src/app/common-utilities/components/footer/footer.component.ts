import { Component, OnInit, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";
import { Footer } from "src/app/common-utilities/book-store-models/Footer.model";
import { ERROR_MESSAGE } from "../../constants/errors.constants";
import { BookStoreCommonService } from "../../services/book-store-common.service";

@Component({
    selector: "app-footer",
    templateUrl: "./footer.component.html",
    styleUrls: ["./footer.component.scss"],
})
export class FooterComponent implements OnInit, OnDestroy {
    footerData: string = "";
    subscription: Subscription = new Subscription();
    error: string;

    constructor(private service: BookStoreCommonService) {}

    /**
     * on init
     */
    ngOnInit(): void {
        this.getFooterData();
    }

    /**
     * Gets footer data
     */
    getFooterData() {
        this.subscription = this.service.getFooterData().subscribe(
            (data: Footer) => {
                this.footerData = data.footerData;
            },
            (error) => (this.error = ERROR_MESSAGE)
        );
    }

    /**
     * on destroy
     */
    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
