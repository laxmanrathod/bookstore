import { Component, OnDestroy, OnInit } from "@angular/core";
import { BookStoreCommonService } from "../../services/book-store-common.service";
import { Subscription } from "rxjs";
import { ERROR_MESSAGE } from "../../constants/errors.constants";
import { Category } from "./../../book-store-models/Category.model";
@Component({
    selector: "app-categories",
    templateUrl: "./categories.component.html",
    styleUrls: ["./categories.component.scss"],
})
export class CategoriesComponent implements OnInit, OnDestroy {
    constructor(private commonService: BookStoreCommonService) {}
    category: Category;
    subscription: Subscription = new Subscription();
    error: string;

    /**
     * on init
     */
    ngOnInit(): void {
        this.getCategoryData();
    }

    /**
     * Gets category data
     */
    getCategoryData(): void {
        this.subscription = this.commonService.getCategoryData().subscribe(
            (data) => {
                this.category = data;
            },
            (error) => (this.error = ERROR_MESSAGE)
        );
    }

    /**
     * on destroy
     */
    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }
}
