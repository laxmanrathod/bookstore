export const ERROR_MESSAGE = "Sorry something went wrong";
export const REQUIRED_ERROR = "required field";
export const LENGTH_VALIDATOR_ERROR = "field must contain 8 characters";
