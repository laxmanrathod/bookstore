import { Component } from "@angular/core";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Amplify } from "@aws-amplify/core";
import { Auth } from "aws-amplify";
import { MatSnackBar } from "@angular/material/snack-bar";
import { Router } from "@angular/router";
import { REQUIRED_ERROR, LENGTH_VALIDATOR_ERROR } from "../../../../app/common-utilities/constants/errors.constants";

Amplify.configure({
    Auth: {
        region: "us-east-1",
        userPoolId: "us-east-1_gFySFruXg",
        userPoolWebClientId: "27lmrglbaioqpoc0q666mm8mhr",
        authenticationFlowType: "USER_PASSWORD_AUTH",
    },
});

@Component({
    selector: "app-login",
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.scss"],
})
export class LoginComponent {
    constructor(private _snackbar: MatSnackBar, private router: Router) {}
    requiredError: string = REQUIRED_ERROR;
    lengthValidatorError: string = LENGTH_VALIDATOR_ERROR;

    login: FormGroup = new FormGroup({
        username: new FormControl("", [Validators.required]),
        password: new FormControl("", [Validators.required, Validators.minLength(8)]),
    });

    /**
     * on submiting the form data
     */
    onSubmit() {
        if (this.login.valid) {
            Auth.signIn(this.login.value.username, this.login.value.password)
                .then((user) => {
                    if (user.challengeName === "NEW_PASSWORD_REQUIRED") {
                        const { requiredAttributes } = user.challengeParam;
                        Auth.completeNewPassword(this.login.value.username, "Test@12345");
                    } else {
                        this.router.navigate(["/dashboard"]);
                    }
                })
                .catch((e) => {
                    this._snackbar.open("Invalid username or password", "ok", {
                        duration: 5000,
                        horizontalPosition: "center",
                        verticalPosition: "top",
                        panelClass: "error",
                    });
                });
        }
    }
}
