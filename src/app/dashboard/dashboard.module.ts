import { AuthorState } from "./../common-utilities/store/states/author.state";
import { CategoriesComponent } from "./../common-utilities/components/categories/categories.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DashboardComponent } from "./dashboard.component";
import { BookListComponent } from "./book-list/book-list.component";
import { CommonUtilitiesModule } from "../common-utilities/common-utilities.module";
import { AuthorsListComponent } from "../common-utilities/components/authors-list/authors-list.component";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatIconModule } from "@angular/material/icon";
import { MatListModule } from "@angular/material/list";
import { MatButtonModule } from "@angular/material/button";
import { HttpClientModule } from "@angular/common/http";
import { FlexLayoutModule } from "@angular/flex-layout";
import { FormsModule } from "@angular/forms";
import { MatCardModule } from "@angular/material/card";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatSidenavModule } from "@angular/material/sidenav";
import { AuthorTileComponent } from "../common-utilities/components/author-tile/author-tile.component";
import { CategoryTilesComponent } from "../common-utilities/components/category-tiles/category-tiles.component";

@NgModule({
    declarations: [DashboardComponent, CategoriesComponent, AuthorsListComponent, AuthorTileComponent, CategoryTilesComponent, BookListComponent],
    imports: [
        CommonModule,
        DashboardRoutingModule,
        MatToolbarModule,
        MatIconModule,
        MatListModule,
        MatButtonModule,
        MatSidenavModule,
        HttpClientModule,
        MatCardModule,
        FlexLayoutModule,
        FormsModule,
        MatGridListModule,
        MatPaginatorModule,
        CommonUtilitiesModule,
    ],
    exports: [BookListComponent],
})
export class DashboardModule {}
